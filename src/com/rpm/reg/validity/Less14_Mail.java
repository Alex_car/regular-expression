package com.rpm.reg.validity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Less14_Mail {
    public static void main(String[] args) {
        int counter = 0;
        String email = "example@mail.ru";
        Pattern pattern = Pattern.compile("^((\\w|[-+])+(\\.[\\w-]+)*@[\\w-]+(\\.[\\d\\p{Alpha}]+)*)$");
        Matcher matcher = pattern.matcher(email);

        while (matcher.find()) {
            counter++;
            System.out.println("Match found '" +
                    email.substring(matcher.start(), matcher.end()) +
                    "'. Starting at index " + matcher.start() +
                    " and ending at index " + matcher.end());
        }
        System.out.println("Matches found: " + counter);
    }
}
