package com.rpm.reg.validity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Less13_MacAddress {
    public static void main(String[] args) {
        int counter = 0;
        String MAC = "77:a3:d2:01:ff:63";
        Pattern pattern = Pattern.compile("^(((\\p{XDigit}{2})[:-]){5}\\p{XDigit}{2})$");
        Matcher matcher = pattern.matcher(MAC);

        while (matcher.find()) {
            counter++;
            System.out.println("Match found '" +
                    MAC.substring(matcher.start(), matcher.end()) +
                    "'. Starting at index " + matcher.start() +
                    " and ending at index " + matcher.end()
            );
        }
        System.out.println("Matches found: " + counter);
    }
}
