package com.rpm.reg;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Less2_LiteralsAndMetacharacters {
    public static void main(String[] args) {
        Pattern pattern1 = Pattern.compile("^ \\f\\n\\r\\t\\v");

        Matcher matcher1 = pattern1.matcher("a b c d e f g h");
        System.out.println(matcher1.find());

        Matcher matcher2 = pattern1.matcher("f g h a b c");
        System.out.println(matcher2.find());


        Matcher matcher3 = pattern1.matcher("2");
        System.out.println(matcher3.find());
    }
}
