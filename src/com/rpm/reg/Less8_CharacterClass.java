package com.rpm.reg;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Less8_CharacterClass {
    public static void main(String[] args) {
        int counter = 0;
        String string = "!@#$%^&*abcd1234";
        Pattern pattern = Pattern.compile("\\p{Punct}");
        Matcher matcher = pattern.matcher(string);

        while (matcher.find()) {
            counter++;
            System.out.println("Match found '" +
                    string.substring(matcher.start(), matcher.end()) +
                    "'. Starting at index " + matcher.start() +
                    " and ending at index " + matcher.end());
        }
        System.out.println("Matches found: " + counter);
    }
}
